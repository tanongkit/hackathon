const dotenv = require('dotenv')
const result = dotenv.config()
const express = require('express');
const passport = require('passport');
const LineStrategy = require('../lib').Strategy;
const path = require('path');
const axios = require('axios')
const qs = require('qs')
var jwt = require('jsonwebtoken');
const uuidv1 = require('uuid/v1');
var swig = require('swig')
var redis = require("redis")

//config
var redis_config = {
  host: process.env.REDIS_HOST,
  port: '6379'
}

var passport_config = {
  channelID: process.env.CHANNEL_ID,
  channelSecret: process.env.CHANNEL_SECRET,
  callbackURL: process.env.CALLBACKURL,
  scope: ['profile', 'openid', 'email'],
  botPrompt: 'aggressive'
}

var line_token = process.env.LINE_TOKEN

// config redis
client = redis.createClient(redis_config);
client.on("error", function (err) {
    console.log("Error " + err);
});
const app = express();

const csv = require('csv-parser');
const fs = require('fs');

const private_key = 'tanongkit'

// This is where all the magic happens!
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(express.static(path.join(__dirname, 'public')));

// callback uri
passport.use(new LineStrategy(passport_config,function(accessToken, refreshToken, params, profile, cb) {
  const {email} = jwt.decode(params.id_token);
  profile.email = email;
  return cb(null, profile);
}));

// Configure Passport authenticated session persistence.
passport.serializeUser(function(user, cb) {cb(null, user);});
passport.deserializeUser(function(obj, cb) {cb(null, obj);});

// Use application-level middleware for common functionality, including
// parsing, and session handling.
app.use(require('body-parser').urlencoded({extended: false}));
app.use(require('body-parser').json());
app.use(require('express-session')({secret: 'keyboard dog', resave: true, saveUninitialized: true}));
// Initialize Passport and restore authentication state, if any, from the session.
app.use(passport.initialize());
app.use(passport.session());


// init product into store
app.get('/init', function(req, res) {
  const stores = {
    1:[],
    2:[]
  }
  let products = []
  fs.createReadStream(__dirname+'/tops_product.csv')
  .pipe(csv())
  .on('data', (row) => {
    const _sku = Math.floor(Math.random() * 1000000000).toString()
    if(!products[_sku]) 
    products[_sku] = {}
    
    products[_sku] = {
      sku: _sku,
      name_th: row.name_th,
      category_en: row.category_en,
      image: row.image,
      price: row.price

    }
    client.set("sku:"+_sku, JSON.stringify(products[_sku]));
  })
  .on('end', () => {
    for(var i=1; i< 3;i++) {
      for(var key in products){
        products[key].stock = Math.floor(Math.random() * 2)
        stores[i].push(products[key].sku)
        stores[i].push(JSON.stringify(products[key]))
      }
    }
    let number = 1
    for(var key in stores){
      client.hmset("top:shop:"+number, stores[key], function (err, data) {
        console.log('init successfully processed');
      });
      number++
    }
    console.log('key' , key)
    res.send('OK');
  });
});
// clear database
app.get('/clear', function(req, res) {
  client.flushdb( function (err, succeeded) {
    res.send('OK');
  });
});
// show for information
app.get('/', function(req, res) {
  console.log(req.user)
  res.render('index', { user: req.user });
});
// check stock and push to line
app.get('/check', function(req, res) {
  console.log("--------check-----------")
  console.log("-------------------")
  if(!req.user){  
    var token = jwt.sign({ params: req.query }, private_key);
    req.session.token = token
    res.redirect('/');
  } else {

    if(req.query.sku){
 
      client.get(req.user.id, function (err, skus) {
        // check existing sku
        let existing_sku = false
        let _skus = JSON.parse(skus)
        if(_skus){
          for(var i=0;i<_skus.length;i++){
            if( _skus[i] == req.query.sku) {
              existing_sku = true
            }
          }
        }
        // check existing_sku
        if(!existing_sku) {

          client.multi([
            ["hgetall", "top:shop:1"],
            ["hgetall", "top:shop:2"]
          ]).exec(function (err, replies) {
            var check = false;
            for(var key in replies){
              for(var key2 in replies[key]){
                let obj = JSON.parse(replies[key][key2])
                if(obj.sku == req.query.sku && obj.stock > 0){
                  check = true
                }
              }
            }
            let msg
            if(check) msg = "สินค้ายังมีเหลือ"
            else msg = "สินค้ายังยังไม่มีในตอนนี้ ถ้าชิ้นนี้ เข้ามาจะรีบติดต่อไป"
            
            var request = require("request");
            var options = { method: 'POST',
              url: 'https://api.line.me/v2/bot/message/push',
              headers: { Authorization: 'Bearer '+line_token, 'Content-Type': 'application/json' },
              body: { 
                to: req.user.id,
                "messages":[{ "type":"text", "text": msg}
                ],
              },
              json: true 
            };
        
            request(options, function (error, response, body) {
              if (error) throw new Error(error);
            })
            console.log('check' , check)
            if(check){
              var request = require("request");
              var options = { method: 'POST',
                url: 'https://api.line.me/v2/bot/message/push',
                headers: 
                { 
                  Authorization: 'Bearer '+ line_token,
                  'Content-Type': 'application/json' },
                body: { 
                  to: req.user.id,
                  "messages": [{
                    "type": "flex",
                    "altText": "สวัสดี คุณ "+req.user.displayName,
                    "contents": {
                      "type": "bubble",
                      "header": {
                        "type": "box",
                        "layout": "vertical",
                        "contents": [
                          {
                            "type": "text",
                            "text": "สวัสดีคุณ " + req.user.displayName
                          }
                        ]
                      },
                      "hero": {
                        "type": "image",
                        "url": req.query.img,
                        "size": "full",
                        "aspectRatio": "2:1"
                      },
                      "body": {
                        "type": "box",
                        "layout": "vertical",
                        "contents": [
                          {
                            "type": "text",
                            "text": "คุณ ต้องการ booking สินค้าชิ้นนี้ ?"
                          }
                        ]
                      },
                      "footer": {
                        "type": "box",
                        "layout": "vertical",
                        "contents": [
                          {
                          "type": "button",
                          "style": "primary",
                          "height": "sm",
                          "action": {
                              "type": "postback",
                              "label": "จองเลย!",
                              "data": "action=reserve&sku="+req.query.sku,
                              "displayText": "Reserve!"
                          }
                          }
                        ]
                      }
                    }
                  }],
                },
                json: true 
              };
              
              console.log('before -- request')
              request(options, function (error, response, body) {
                if (error) throw new Error(error);
                console.log('response')
                client.get(req.user.id, function (err, skus) {
                  if (err) throw(err);
    
              
                  req.query.status = "watch"
                  if (skus) {
                  
                    let _skus = JSON.parse(skus)
                    _skus.push(req.query.sku)
                    client.del(req.user.id)
                    client.set(req.user.id, JSON.stringify(_skus));
                    client.set(req.user.id+":"+req.query.sku, JSON.stringify(req.query));
                    //res.redirect('/tops');
                  } else {
              
                    let _skus = []
                    _skus.push(req.query.sku)
                    client.set(req.user.id, JSON.stringify(_skus));
                    client.set(req.user.id+":"+req.query.sku, JSON.stringify(req.query));
                  }
                  client.get("user:topshop", function (err, users) {
                    let existing_user = false
                    let _users = JSON.parse(users)
                    if(_users){
                      for(var i=0;i<_users.length;i++){
                        if( _users[i] == req.user.id) {
                          existing_user = true
                        }
                      }
                    } else _users = []
    
                    if(!existing_user) {
                      _users.push(req.user.id)
                      client.set("user:topshop", JSON.stringify(_users));
                    }
                    if(req.session.store)
                      res.redirect('/tops?store='+req.session.store);
                    else
                      res.redirect('/tops');
                  });
                });
    
              })
            } else {

              client.get(req.user.id, function (err, skus) {
                if (err) throw(err);
  
                req.query.status = "watch_out_of_stock"
                //console.log('req.query', req.query)
                if (skus) {
                  console.log('if')
                  let _skus = JSON.parse(skus)
                  _skus.push(req.query.sku)
                  client.del(req.user.id)
                  client.set(req.user.id, JSON.stringify(_skus));
                  client.set(req.user.id+":"+req.query.sku, JSON.stringify(req.query));
                  //res.redirect('/tops');
                } else {
                  console.log('else')
                  let _skus = []
                  _skus.push(req.query.sku)
                  client.set(req.user.id, JSON.stringify(_skus));
                  client.set(req.user.id+":"+req.query.sku, JSON.stringify(req.query));
                }
          
                client.get("user:topshop", function (err, users) {
                  let existing_user = false
                  let _users = JSON.parse(users)
    
                  if(_users){
                    for(var i=0;i<_users.length;i++){
                      if( _users[i] == req.user.id) {
                        existing_user = true
                      }
                    }
                  } else _users = []
                  if(!existing_user) {
                    _users.push(req.user.id)
                    client.set("user:topshop", JSON.stringify(_users));
                  }
                  res.redirect('/tops');
                });
              });

              //res.redirect('/tops');
            }

          });

        } else {
          var request = require("request");
          var options = { method: 'POST',
            url: 'https://api.line.me/v2/bot/message/push',
            headers: { Authorization: 'Bearer '+line_token, 'Content-Type': 'application/json' },
            body: { 
              to: req.user.id,
              "messages":[{ "type":"text", "text":"คุณจองแล้ว please wait feedback!"}
              ],
            },
            json: true 
          };
      
          request(options, function (error, response, body) {
            if (error) throw new Error(error);
            if(req.session.store)
              res.redirect('/tops?store='+req.session.store);
            else
              res.redirect('/tops');
          })
        }
      });
    } else {
      res.redirect('/tops');
    }
  }
});
// show store
app.get('/tops', function(req, res) {
  console.log('/tops')
  let store = 1
  if(req.query.store) store = req.query.store
  const items = []
  req.session.store = store
  client.hgetall("top:shop:"+ store, function(err, products){
    if(err) console.log('ERR',err)
    
    for (var key in products) {
      items.push(JSON.parse(products[key]))
    }
    res.render('tops',{ 
      user: req.user ,
      items: items
    });

  });
});

app.get('/cds', function(req, res) {

  let store = 1
  if(req.query.store) store = req.query.store
  const items = []
  req.session.store = store
  client.hgetall("top:shop:"+ store, function(err, products){
    if(err) console.log('ERR',err)
    
    for (var key in products) {
      items.push(JSON.parse(products[key]))
    }

    res.render('cds',{ 
      user: req.user ,
      items: items
    });

  });
});


// show product on store
app.get('/store', function(req, res) {
  let shops = []
  client.multi([
    ["hgetall", "top:shop:1"],
    ["hgetall", "top:shop:2"]
  ]).exec(function (err, replies) {
    var number = 1;
    for(var key in replies){
      let stores = {}
      stores = {
        store : "top:shop:"+number
      }
      stores.items = []
      for(var key2 in replies[key]){
        let obj = JSON.parse(replies[key][key2])
        stores.items.push({
          sku : key2,
          stock : obj.stock,
          image: obj.image,
          name_th: obj.name_th
        })
      }
      shops.push(stores)
      number++
    }
    res.render('store',{ 
      shops: shops
    });
  });
});
// login with line
app.get('/login/line', passport.authenticate('line'));
// call back from line
app.get('/callback', passport.authenticate('line', {failureRedirect: '/'}), function(req, res) {
  console.log("--------callback success-----------", req.session.token)
  let query = jwt.verify(req.session.token, private_key);
  console.log('query' , query)
  let serialize = function(obj) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }(query.params)
  res.redirect('/check?'+serialize);
});
// get all watchlists
app.get('/watchlists', function(req, res) {
  console.log('req.user' , req.user)
  client.get("user:topshop", function (err, users) {
    let a = []
    if(users) {
      client.multi([
        ["get", "U02be89b0bbfa25928afca21602573828"]
      ]).exec(function (err, replies) {
        if(replies) {
          let b = JSON.parse(replies)
          let c = []
          for (var i=0;i<b.length;i++){
            c.push(
              ["get", "U02be89b0bbfa25928afca21602573828:"+b[i]]
            )
          }      
      
          client.multi(c).exec(function (err, replies2) {
            console.log('err',err)
            console.log('replies2',replies2)
            for (var i=0;i<replies2.length;i++){
              let d = JSON.parse(replies2[i])
              a.push({
                transaction : "U02be89b0bbfa25928afca21602573828:325235326",
                user: "U02be89b0bbfa25928afca21602573828",
                sku: d.sku,
                image: d.img,
                status: d.status ?  d.status : "watch"
              })
            }
            console.log(a);
            res.render('watchlist',{ 
              items: a
            });
          });
        }else {
          res.render('watchlist',{ 
            items: []
          });
        }
        //console.log(replies);
      });
    }else {
      res.render('watchlist',{ 
        items: []
      });
    }
  })
});

//api
app.get('/checkstock/:sku', function(req, res) {
  let sku = req.params.sku
  client.multi([
    ["hgetall", "top:shop:1"],
    ["hgetall", "top:shop:2"]
  ]).exec(function (err, replies) {
    var check = false;
    for(var key in replies){
      for(var key2 in replies[key]){
        let obj = JSON.parse(replies[key][key2])
        if(obj.sku == sku && obj.stock > 0) check = true
      }
    }
    res.json({      
      balance_stock : check
    });
  })
});

app.get('/watchlist/:uid', function(req, res) {
  console.log('API watchlist ----')
  console.log('req.param : ',req.params)
  let uid = req.params.uid
  let products = []
  client.get(uid, function (err, skus){
    console.log(skus)
    if(skus) {
      let gets = []
      let obj = JSON.parse(skus)
      for(var i=0;i<obj.length;i++){
        let set = []
        set.push("get")
        set.push(uid+":"+obj[i])
        gets.push(set)
      }
     // console.log('get', gets)
      client.multi(gets).exec(function (err, replies) {
        console.log('replies', replies);
        for(var k in replies){
          products.push(JSON.parse(replies[k]))
        }
        console.log(products)
        //products.push(replies)
        res.json({      
          data : products
        });
      })
    } else {
      res.json({      
        data : []
      });
    }

  });

});

app.put('/stock/:sku/store/:store_id', function(req, res) {
  console.log('API watchlist ----')
  console.log('req.param : ',req.params)
  let sku = req.params.sku
  let store_id = req.params.store_id
  let stock = 1
  let items = []
  let uid
  client.hgetall("top:shop:"+store_id, function (err, products){
    for (var key in products) {
      let obj = JSON.parse(products[key])
      if(key == sku) {
        obj.stock = stock
      }
      items.push(key)
      items.push(JSON.stringify(obj))
    }
    
    client.del("top:shop:"+store_id)
    client.hmset("top:shop:"+store_id, items, function (err, data) {
      
      // push 
      client.get("user:topshop", function (err, uids){
        if(uids) {
          let obj_uids = JSON.parse(uids)
          
          for(var i=0;i< obj_uids.length;i++){
            
            client.get(obj_uids[i]+":"+sku, function (err, product){
              if(product){
                
                //let obj_product = JSON.parse(product)
                console.log('obj_product', JSON.parse(product).status)
                if(JSON.parse(product).status == 'watch_out_of_stock'){
                  
                  var request = require("request");
                  var options = { method: 'POST',
                    url: 'https://api.line.me/v2/bot/message/push',
                    headers: {  Authorization: 'Bearer '+ line_token, 'Content-Type': 'application/json' },
                    body: { 
                      to: obj_uids[0],
                      "messages": [{
                        "type": "flex",
                        "altText": "สินค้ามาแล้วจ้าา",
                        "contents": {
                          "type": "bubble",
                          "header": {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                              {
                                "type": "text",
                                "text": "สินค้ามาแล้วจ้าา"
                              }
                            ]
                          },
                          "hero": {
                            "type": "image",
                            "url": JSON.parse(product).img,
                            "size": "full",
                            "aspectRatio": "2:1"
                          },
                          "body": {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                              {
                                "type": "text",
                                "text": "คุณ ต้องการ booking สินค้าชิ้นนี้ ?"
                              }
                            ]
                          },
                          "footer": {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                              {
                              "type": "button",
                              "style": "primary",
                              "height": "sm",
                              "action": {
                                  "type": "postback",
                                  "label": "จองเลย!",
                                  "data": "action=reserve&sku="+JSON.parse(product).sku,
                                  "displayText": "Reserve!"
                              }
                              }
                            ]
                          }
                        }
                      }],
                    },
                    json: true 
                  };
                  console.log('options' , options)
                  request(options, function (error, response, body) {
                    console.log('done')
                    res.json({
                      status : true
                    })
                  });

                } else {
                  res.json({
                    status : true
                  })
                }
              } else {
                res.json({
                  status : true
                })
              }
            });
          }
        }
      })
    });
  });

});

app.put('/watchlist/:id/:status', function(req, res) {
  console.log('API watchlist BY ID ----')
  console.log('req.param : ',req.params)
  let watchlist_id = req.params.id
  let status = req.params.status
  client.get(watchlist_id, function (err, product){
   // console.log(product)
    if(product) {
      let obj = JSON.parse(product)
      obj.status = status
      client.del(watchlist_id)
      client.set(watchlist_id, JSON.stringify(obj));
      client.get(watchlist_id, function (err, a){
        res.json({
          status : true,
          product: a
        })
      })
    } else {
      res.json({
        status : false
      })
    }
  });
});

app.get('/watchlist/:id', function(req, res) {
  console.log('API watchlist BY ID ----')
  console.log('req.param : ',req.params)
  let watchlist_id = req.params.id
  client.get(watchlist_id, function (err, product){
    let obj = JSON.parse(product)
    client.get(watchlist_id, function (err, a){
      res.json({
        status : true,
        product: obj
      })
    })
  });

});

app.listen(3000, () => console.log('Example app listening on http://localhost:3000!'))
